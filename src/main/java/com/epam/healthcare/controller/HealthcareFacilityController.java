package com.epam.healthcare.controller;

import com.epam.healthcare.entity.HealthcareFacility;
import com.epam.healthcare.service.HealthcareFacilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/healthcarefacilities")
public class HealthcareFacilityController {

    private final HealthcareFacilityService healthcareFacilityService;

    @Autowired
    public HealthcareFacilityController(HealthcareFacilityService healthcareFacilityService) {
        this.healthcareFacilityService = healthcareFacilityService;
    }

    @PostMapping
    public ResponseEntity<HealthcareFacility> addHealthcareFacility(@RequestBody HealthcareFacility healthcareFacility) {
        HealthcareFacility savedHealthcareFacility = healthcareFacilityService.saveHealthcareFacility(healthcareFacility);
        return new ResponseEntity<>(savedHealthcareFacility, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<HealthcareFacility> getHealthcareFacilityById(@PathVariable Long id) {
        return healthcareFacilityService.findById(id)
                .map(healthcareFacility -> new ResponseEntity<>(healthcareFacility, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping
    public List<HealthcareFacility> getAllHealthcareFacilities() {
        return healthcareFacilityService.findAllHealthcareFacilities();
    }

    @PutMapping("/{id}")
    public ResponseEntity<HealthcareFacility> updateHealthcareFacility(@PathVariable Long id, @RequestBody HealthcareFacility healthcareFacility) {
        return healthcareFacilityService.findById(id)
                .map(existingFacility -> {
                    healthcareFacility.setId(id);
                    HealthcareFacility updatedHealthcareFacility = healthcareFacilityService.updateHealthcareFacility(healthcareFacility);
                    return new ResponseEntity<>(updatedHealthcareFacility, HttpStatus.OK);
                })
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteHealthcareFacility(@PathVariable Long id) {
        healthcareFacilityService.deleteHealthcareFacility(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}

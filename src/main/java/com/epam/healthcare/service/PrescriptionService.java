package com.epam.healthcare.service;

import com.epam.healthcare.entity.Prescription;

import java.util.List;
import java.util.Optional;

public interface PrescriptionService {

    Prescription savePrescription(Prescription prescription);
    Optional<Prescription> findById(Long id);
    List<Prescription> findAllPrescriptions();
    Prescription updatePrescription(Prescription prescription);
    void deletePrescription(Long id);

}

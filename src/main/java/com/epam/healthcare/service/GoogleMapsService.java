package com.epam.healthcare.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class GoogleMapsService {

    @Value("${google.maps.api.key}")
    private String googleMapsApiKey;

    public String getDirections(String origin, String destination) {
        String GOOGLE_MAPS_ENDPOINT = "https://maps.googleapis.com/maps/api/directions/json";

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(GOOGLE_MAPS_ENDPOINT)
                .queryParam("origin", origin)
                .queryParam("destination", destination)
                .queryParam("key", googleMapsApiKey);

        RestTemplate restTemplate = new RestTemplate();

        return restTemplate.getForObject(builder.toUriString(), String.class);
    }

}
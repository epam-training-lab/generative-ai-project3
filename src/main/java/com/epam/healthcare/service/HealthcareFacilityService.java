package com.epam.healthcare.service;

import com.epam.healthcare.entity.HealthcareFacility;

import java.util.List;
import java.util.Optional;

public interface HealthcareFacilityService {

    HealthcareFacility saveHealthcareFacility(HealthcareFacility healthcareFacility);
    Optional<HealthcareFacility> findById(Long id);
    List<HealthcareFacility> findAllHealthcareFacilities();
    HealthcareFacility updateHealthcareFacility(HealthcareFacility healthcareFacility);
    void deleteHealthcareFacility(Long id);

}

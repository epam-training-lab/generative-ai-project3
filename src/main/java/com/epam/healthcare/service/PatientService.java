package com.epam.healthcare.service;

import com.epam.healthcare.entity.Patient;

import java.util.List;
import java.util.Optional;

public interface PatientService {

    Patient savePatient(Patient patient);
    Optional<Patient> findById(Long id);
    List<Patient> findAllPatients();
    Patient updatePatient(Patient patient);
    void deletePatient(Long id);

}
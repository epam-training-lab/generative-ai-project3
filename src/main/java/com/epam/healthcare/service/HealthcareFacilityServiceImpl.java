package com.epam.healthcare.service;

import com.epam.healthcare.entity.HealthcareFacility;
import com.epam.healthcare.repository.HealthcareFacilityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HealthcareFacilityServiceImpl implements HealthcareFacilityService {

    private final HealthcareFacilityRepository healthcareFacilityRepository;

    @Autowired
    public HealthcareFacilityServiceImpl(HealthcareFacilityRepository healthcareFacilityRepository) {
        this.healthcareFacilityRepository = healthcareFacilityRepository;
    }

    @Override
    public HealthcareFacility saveHealthcareFacility(HealthcareFacility healthcareFacility) {
        return healthcareFacilityRepository.save(healthcareFacility);
    }

    @Override
    public Optional<HealthcareFacility> findById(Long id) {
        return healthcareFacilityRepository.findById(id);
    }

    @Override
    public List<HealthcareFacility> findAllHealthcareFacilities() {
        return healthcareFacilityRepository.findAll();
    }

    @Override
    public HealthcareFacility updateHealthcareFacility(HealthcareFacility healthcareFacility) {
        return healthcareFacilityRepository.save(healthcareFacility);
    }

    @Override
    public void deleteHealthcareFacility(Long id) {
        healthcareFacilityRepository.deleteById(id);
    }

}
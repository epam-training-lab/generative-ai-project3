package com.epam.healthcare.service;

import com.epam.healthcare.entity.Appointment;

import java.util.List;
import java.util.Optional;

public interface AppointmentService {

    Appointment saveAppointment(Appointment appointment);
    Optional<Appointment> findById(Long id);
    List<Appointment> findAllAppointments();
    Appointment updateAppointment(Appointment appointment);
    void deleteAppointment(Long id);

}

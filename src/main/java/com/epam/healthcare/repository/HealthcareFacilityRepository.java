package com.epam.healthcare.repository;

import com.epam.healthcare.entity.HealthcareFacility;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HealthcareFacilityRepository extends JpaRepository<HealthcareFacility, Long> {

}

package com.epam.healthcare.controller;

import com.epam.healthcare.entity.HealthcareFacility;
import com.epam.healthcare.service.HealthcareFacilityService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(HealthcareFacilityController.class)
class HealthcareFacilityControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HealthcareFacilityService healthcareFacilityService;

    private HealthcareFacility healthcareFacility;

    @BeforeEach
    void setUp() {
        healthcareFacility = new HealthcareFacility();
        healthcareFacility.setId(1L);
    }

    @Test
    void addHealthcareFacility() throws Exception {
        given(healthcareFacilityService.saveHealthcareFacility(any(HealthcareFacility.class))).willReturn(healthcareFacility);

        mockMvc.perform(post("/api/healthcarefacilities")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(healthcareFacility)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(healthcareFacility.getId()));
    }

    @Test
    void getHealthcareFacilityById() throws Exception {
        given(healthcareFacilityService.findById(healthcareFacility.getId())).willReturn(Optional.of(healthcareFacility));

        mockMvc.perform(get("/api/healthcarefacilities/{id}", healthcareFacility.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(healthcareFacility.getId()));
    }

    @Test
    void getAllHealthcareFacilities() throws Exception {
        List<HealthcareFacility> facilities = Arrays.asList(healthcareFacility);
        given(healthcareFacilityService.findAllHealthcareFacilities()).willReturn(facilities);

        mockMvc.perform(get("/api/healthcarefacilities"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(healthcareFacility.getId()));
    }

    @Test
    void updateHealthcareFacility() throws Exception {
        given(healthcareFacilityService.findById(healthcareFacility.getId())).willReturn(Optional.of(healthcareFacility));
        given(healthcareFacilityService.updateHealthcareFacility(any(HealthcareFacility.class))).willReturn(healthcareFacility);

        mockMvc.perform(put("/api/healthcarefacilities/{id}", healthcareFacility.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(healthcareFacility)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(healthcareFacility.getId()));
    }

    @Test
    void deleteHealthcareFacility() throws Exception {
        doNothing().when(healthcareFacilityService).deleteHealthcareFacility(healthcareFacility.getId());

        mockMvc.perform(delete("/api/healthcarefacilities/{id}", healthcareFacility.getId()))
                .andExpect(status().isNoContent());
    }

}
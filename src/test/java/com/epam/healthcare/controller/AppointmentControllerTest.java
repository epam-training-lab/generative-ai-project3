package com.epam.healthcare.controller;

import com.epam.healthcare.entity.Appointment;
import com.epam.healthcare.service.AppointmentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AppointmentController.class)
class AppointmentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AppointmentService appointmentService;

    private Appointment appointment;

    @BeforeEach
    void setUp() {
        appointment = new Appointment();
        appointment.setId(1L);
    }

    @Test
    void addAppointment() throws Exception {
        given(appointmentService.saveAppointment(appointment)).willReturn(appointment);

        mockMvc.perform(post("/api/appointments")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(appointment)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(appointment.getId()));
    }

    @Test
    void getAppointmentById() throws Exception {
        given(appointmentService.findById(appointment.getId())).willReturn(Optional.of(appointment));

        mockMvc.perform(get("/api/appointments/{id}", appointment.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(appointment.getId()));
    }

    @Test
    void getAllAppointments() throws Exception {
        given(appointmentService.findAllAppointments()).willReturn(Collections.singletonList(appointment));

        mockMvc.perform(get("/api/appointments"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(appointment.getId()));
    }

    @Test
    void updateAppointment() throws Exception {
        given(appointmentService.findById(appointment.getId())).willReturn(Optional.of(appointment));
        given(appointmentService.updateAppointment(appointment)).willReturn(appointment);

        mockMvc.perform(put("/api/appointments/{id}", appointment.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(appointment)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(appointment.getId()));
    }

    @Test
    void deleteAppointment() throws Exception {
        doNothing().when(appointmentService).deleteAppointment(appointment.getId());

        mockMvc.perform(delete("/api/appointments/{id}", appointment.getId()))
                .andExpect(status().isNoContent());
    }

}
package com.epam.healthcare.controller;

import com.epam.healthcare.entity.Prescription;
import com.epam.healthcare.service.PrescriptionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PrescriptionController.class)
class PrescriptionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PrescriptionService prescriptionService;

    private Prescription prescription;

    @BeforeEach
    void setUp() {
        prescription = new Prescription();
        prescription.setId(1L);
    }

    @Test
    void addPrescription() throws Exception {
        given(prescriptionService.savePrescription(any(Prescription.class))).willReturn(prescription);

        mockMvc.perform(post("/api/prescriptions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(prescription)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(prescription.getId().intValue()));
    }

    @Test
    void getPrescriptionById() throws Exception {
        given(prescriptionService.findById(prescription.getId())).willReturn(Optional.of(prescription));

        mockMvc.perform(get("/api/prescriptions/{id}", prescription.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(prescription.getId().intValue()));
    }

    @Test
    void getAllPrescriptions() throws Exception {
        List<Prescription> prescriptions = Collections.singletonList(prescription);
        given(prescriptionService.findAllPrescriptions()).willReturn(prescriptions);

        mockMvc.perform(get("/api/prescriptions"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(prescription.getId().intValue()));
    }

    @Test
    void updatePrescription() throws Exception {
        given(prescriptionService.findById(prescription.getId())).willReturn(Optional.of(prescription));
        given(prescriptionService.updatePrescription(any(Prescription.class))).willReturn(prescription);

        mockMvc.perform(put("/api/prescriptions/{id}", prescription.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(prescription)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(prescription.getId().intValue()));
    }

    @Test
    void deletePrescription() throws Exception {
        doNothing().when(prescriptionService).deletePrescription(prescription.getId());

        mockMvc.perform(delete("/api/prescriptions/{id}", prescription.getId()))
                .andExpect(status().isNoContent());
    }

}
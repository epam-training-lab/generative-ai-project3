package com.epam.healthcare.controller;

import com.epam.healthcare.entity.Patient;
import com.epam.healthcare.service.PatientService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PatientController.class)
class PatientControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PatientService patientService;

    private Patient patient;

    @BeforeEach
    void setUp() {
        patient = new Patient();
        patient.setId(1L);
    }

    @Test
    void addPatient() throws Exception {
        given(patientService.savePatient(any(Patient.class))).willReturn(patient);

        mockMvc.perform(post("/api/patients")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(patient)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(patient.getId().intValue()));
    }

    @Test
    void getPatientById() throws Exception {
        given(patientService.findById(patient.getId())).willReturn(Optional.of(patient));

        mockMvc.perform(get("/api/patients/{id}", patient.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(patient.getId().intValue()));
    }

    @Test
    void getAllPatients() throws Exception {
        List<Patient> patients = Collections.singletonList(patient);
        given(patientService.findAllPatients()).willReturn(patients);

        mockMvc.perform(get("/api/patients"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(patient.getId().intValue()));
    }

    @Test
    void updatePatient() throws Exception {
        given(patientService.findById(patient.getId())).willReturn(Optional.of(patient));
        given(patientService.updatePatient(any(Patient.class))).willReturn(patient);

        mockMvc.perform(put("/api/patients/{id}", patient.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(patient)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(patient.getId().intValue()));
    }

    @Test
    void deletePatient() throws Exception {
        doNothing().when(patientService).deletePatient(patient.getId());

        mockMvc.perform(delete("/api/patients/{id}", patient.getId()))
                .andExpect(status().isNoContent());
    }

}
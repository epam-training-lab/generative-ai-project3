package com.epam.healthcare.service;

import com.epam.healthcare.entity.HealthcareFacility;
import com.epam.healthcare.repository.HealthcareFacilityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class HealthcareFacilityServiceImplTest {

    @Mock
    private HealthcareFacilityRepository healthcareFacilityRepository;

    @InjectMocks
    private HealthcareFacilityServiceImpl healthcareFacilityService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void saveHealthcareFacility() {
        HealthcareFacility healthcareFacility = new HealthcareFacility();
        when(healthcareFacilityRepository.save(healthcareFacility)).thenReturn(healthcareFacility);

        HealthcareFacility savedHealthcareFacility = healthcareFacilityService.saveHealthcareFacility(healthcareFacility);
        assertEquals(healthcareFacility, savedHealthcareFacility);
        verify(healthcareFacilityRepository).save(healthcareFacility);
    }

    @Test
    void findById() {
        Long id = 1L;
        HealthcareFacility healthcareFacility = new HealthcareFacility();
        when(healthcareFacilityRepository.findById(id)).thenReturn(Optional.of(healthcareFacility));

        Optional<HealthcareFacility> found = healthcareFacilityService.findById(id);
        assertTrue(found.isPresent());
        assertEquals(healthcareFacility, found.get());
        verify(healthcareFacilityRepository).findById(id);
    }

    @Test
    void findAllHealthcareFacilities() {
        List<HealthcareFacility> healthcareFacilities = Arrays.asList(new HealthcareFacility(), new HealthcareFacility());
        when(healthcareFacilityRepository.findAll()).thenReturn(healthcareFacilities);

        List<HealthcareFacility> foundHealthcareFacilities = healthcareFacilityService.findAllHealthcareFacilities();
        assertEquals(2, foundHealthcareFacilities.size());
        verify(healthcareFacilityRepository).findAll();
    }

    @Test
    void updateHealthcareFacility() {
        HealthcareFacility healthcareFacility = new HealthcareFacility();
        when(healthcareFacilityRepository.save(healthcareFacility)).thenReturn(healthcareFacility);

        HealthcareFacility updatedHealthcareFacility = healthcareFacilityService.updateHealthcareFacility(healthcareFacility);
        assertEquals(healthcareFacility, updatedHealthcareFacility);
        verify(healthcareFacilityRepository).save(healthcareFacility);
    }

    @Test
    void deleteHealthcareFacility() {
        Long id = 1L;
        doNothing().when(healthcareFacilityRepository).deleteById(id);

        healthcareFacilityService.deleteHealthcareFacility(id);
        verify(healthcareFacilityRepository).deleteById(id);
    }

}
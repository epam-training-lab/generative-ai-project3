package com.epam.healthcare.service;

import com.epam.healthcare.entity.Prescription;
import com.epam.healthcare.repository.PrescriptionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class PrescriptionServiceImplTest {

    @Mock
    private PrescriptionRepository prescriptionRepository;

    @InjectMocks
    private PrescriptionServiceImpl prescriptionService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void savePrescription() {
        Prescription prescription = new Prescription();
        when(prescriptionRepository.save(prescription)).thenReturn(prescription);

        Prescription savedPrescription = prescriptionService.savePrescription(prescription);
        assertEquals(prescription, savedPrescription);
        verify(prescriptionRepository).save(prescription);
    }

    @Test
    void findById() {
        Long id = 1L;
        Prescription prescription = new Prescription();
        when(prescriptionRepository.findById(id)).thenReturn(Optional.of(prescription));

        Optional<Prescription> found = prescriptionService.findById(id);
        assertTrue(found.isPresent());
        assertEquals(prescription, found.get());
        verify(prescriptionRepository).findById(id);
    }

    @Test
    void findAllPrescriptions() {
        List<Prescription> prescriptions = Arrays.asList(new Prescription(), new Prescription());
        when(prescriptionRepository.findAll()).thenReturn(prescriptions);

        List<Prescription> foundPrescriptions = prescriptionService.findAllPrescriptions();
        assertEquals(2, foundPrescriptions.size());
        verify(prescriptionRepository).findAll();
    }

    @Test
    void updatePrescription() {
        Prescription prescription = new Prescription();
        when(prescriptionRepository.save(prescription)).thenReturn(prescription);

        Prescription updatedPrescription = prescriptionService.updatePrescription(prescription);
        assertEquals(prescription, updatedPrescription);
        verify(prescriptionRepository).save(prescription);
    }

    @Test
    void deletePrescription() {
        Long id = 1L;
        doNothing().when(prescriptionRepository).deleteById(id);

        prescriptionService.deletePrescription(id);
        verify(prescriptionRepository).deleteById(id);
    }

}
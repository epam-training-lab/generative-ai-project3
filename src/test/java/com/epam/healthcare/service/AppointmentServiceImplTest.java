package com.epam.healthcare.service;

import com.epam.healthcare.entity.Appointment;
import com.epam.healthcare.repository.AppointmentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class AppointmentServiceImplTest {

    @Mock
    private AppointmentRepository appointmentRepository;

    @InjectMocks
    private AppointmentServiceImpl appointmentService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void saveAppointment() {
        Appointment appointment = new Appointment(); // Assume Appointment has a no-arg constructor
        when(appointmentRepository.save(appointment)).thenReturn(appointment);

        Appointment savedAppointment = appointmentService.saveAppointment(appointment);
        assertEquals(appointment, savedAppointment);
        verify(appointmentRepository).save(appointment);
    }

    @Test
    void findById() {
        Long id = 1L;
        Appointment appointment = new Appointment(); // Set necessary fields
        when(appointmentRepository.findById(id)).thenReturn(Optional.of(appointment));

        Optional<Appointment> found = appointmentService.findById(id);
        assertTrue(found.isPresent());
        assertEquals(appointment, found.get());
        verify(appointmentRepository).findById(id);
    }

    @Test
    void findAllAppointments() {
        List<Appointment> appointments = Arrays.asList(new Appointment(), new Appointment());
        when(appointmentRepository.findAll()).thenReturn(appointments);

        List<Appointment> foundAppointments = appointmentService.findAllAppointments();
        assertEquals(2, foundAppointments.size());
        verify(appointmentRepository).findAll();
    }

    @Test
    void updateAppointment() {
        Appointment appointment = new Appointment(); // Assume Appointment has a no-arg constructor
        when(appointmentRepository.save(appointment)).thenReturn(appointment);

        Appointment updatedAppointment = appointmentService.updateAppointment(appointment);
        assertEquals(appointment, updatedAppointment);
        verify(appointmentRepository).save(appointment);
    }

    @Test
    void deleteAppointment() {
        Long id = 1L;
        doNothing().when(appointmentRepository).deleteById(id);

        appointmentService.deleteAppointment(id);
        verify(appointmentRepository).deleteById(id);
    }

}
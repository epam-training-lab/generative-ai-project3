package com.epam.healthcare.service;

import com.epam.healthcare.entity.Patient;
import com.epam.healthcare.repository.PatientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class PatientServiceImplTest {

    @Mock
    private PatientRepository patientRepository;

    @InjectMocks
    private PatientServiceImpl patientService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void savePatient() {
        Patient patient = new Patient();
        when(patientRepository.save(patient)).thenReturn(patient);

        Patient savedPatient = patientService.savePatient(patient);
        assertEquals(patient, savedPatient);
        verify(patientRepository).save(patient);
    }

    @Test
    void findById() {
        Long id = 1L;
        Patient patient = new Patient();
        when(patientRepository.findById(id)).thenReturn(Optional.of(patient));

        Optional<Patient> found = patientService.findById(id);
        assertTrue(found.isPresent());
        assertEquals(patient, found.get());
        verify(patientRepository).findById(id);
    }

    @Test
    void findAllPatients() {
        List<Patient> patients = Arrays.asList(new Patient(), new Patient());
        when(patientRepository.findAll()).thenReturn(patients);

        List<Patient> foundPatients = patientService.findAllPatients();
        assertEquals(2, foundPatients.size());
        verify(patientRepository).findAll();
    }

    @Test
    void updatePatient() {
        Patient patient = new Patient();
        when(patientRepository.save(patient)).thenReturn(patient);

        Patient updatedPatient = patientService.updatePatient(patient);
        assertEquals(patient, updatedPatient);
        verify(patientRepository).save(patient);
    }

    @Test
    void deletePatient() {
        Long id = 1L;
        doNothing().when(patientRepository).deleteById(id);

        patientService.deletePatient(id);
        verify(patientRepository).deleteById(id);
    }

}
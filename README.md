# Healthcare Management System

## Description

This project is a Healthcare Management System developed using Spring Boot, designed to help healthcare providers manage patient records, schedule appointments, and prescribe medication. It integrates with the Google Maps API to provide directions to healthcare facilities.

## Features

- **Patient Management**: Add, update, view, and delete patient records.
- **Appointment Scheduling**: Schedule, view, update, and cancel appointments.
- **Prescription Management**: Prescribe, update, and manage medications for patients.
- **Healthcare Facility Directions**: Integrate with Google Maps to provide directions to healthcare facilities.

## Technologies Used

- Spring Boot
- Spring Data JPA
- MySQL
- Google Maps Directions API

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- Java 21 or newer
- Maven
- MySQL Server
- Google Maps API Key

### Run the application:

`java -jar target/healthcare-management-system-0.0.1-SNAPSHOT.jar`

The application should now be running on `http://localhost:8080`.

## Contributing

If you would like to contribute to the development of this project, please follow these steps:

1. Fork the project repository.
2. Create a new branch (`git checkout -b feature/YourFeature`).
3. Commit your changes (`git commit -am 'Add some feature'`).
4. Push to the branch (`git push origin feature/YourFeature`).
5. Create a new Pull Request.